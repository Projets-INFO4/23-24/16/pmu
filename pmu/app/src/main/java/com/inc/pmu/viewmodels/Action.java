package com.inc.pmu.viewmodels;

public class Action {
    public static final String ACTION = "action";
    // Actions
    public static final String PLAYER_USERNAME = "player_username";
    public static final String PLAYER_PUUID = "player_puuid";
    public static final String PLAYER_LIST = "player_list";
    public static final String START_BET = "start_bet";
    public static final String BET = "bet";
    public static final String BET_VALID = "bet_valid";
    public static final String DRAW_CARD = "draw_card";
    public static final String ASK_DO_PUSH_UPS = "ask_do_push_ups";
    public static final String DO_PUSH_UPS = "do_push_ups";
    public static final String CONFIRM_PUSH_UPS = "confirm_push_ups";
    public static final String START_VOTE = "start_vote";
    public static final String START_GAME = "start_game";
    public static final String VOTE = "vote";
    public static final String VOTE_RESULTS = "vote_results";
    public static final String GAME_END = "game_end";
    public static final String GIVE_PUSHUPS = "give_pushups";
    public static final String END_PUSHUPS = "end_pushups";

}
